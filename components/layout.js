import '../pages/_app.js'

function Layout({ children }) {
  return <div className="layout"> { children }
  <style jsx>{`
    .layout{
      background-color:red;
      padding:0;
      margin:0;
      width:100%;
      height:100%;
    }
    `}</style>
  </div>
}

export default Layout
