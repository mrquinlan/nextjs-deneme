import Link from 'next/link'
import Layout from '../../components/layout.js'

export default function FirstPost() {
  return (
    <Layout>
    <h1 className="h1">First Post</h1>
    <h3> <Link href="/"><a>Back to Home</a></Link> </h3>
    <style jsx>{`      
      .h1{
        color:yellow;
      }
      `}</style>
    </Layout>
  )
}
